// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package report

import (
	"dse/gltt/gl"
	"dse/gltt/storage"
	"strings"
	"time"

	"github.com/rodaine/table"
)

func AllEntriesAtIssue(issue gl.Issue) string {
	rb := new(strings.Builder)
	tbl := table.New("", "Name", "Date", "Duration", "Summary").WithWriter(rb)
	for _, te := range issue.Entries {
		tbl.AddRow("", te.User, te.Date.Format("2006-01-02"), te.Duration, te.Summary)
	}
	tbl.AddRow("Total:", "", issue.TotalSpent, "", "")
	tbl.AddRow("Of Estimation:", "", issue.Estimate, "", "")
	tbl.AddRow("Remaining Budget:", "", time.Duration((issue.Estimate - issue.TotalSpent)).String(), "", "")
	tbl.Print()
	result := rb.String()
	return result
}

func isOpenBoolToString(isOpen bool) string {
	if isOpen {
		return "running"
	} else {
		return "stopped"
	}
}

func AllOpenEntries(db *storage.DB) string {
	rb := new(strings.Builder)
	tbl := table.New("Host", "Project-Address", "Issue-ID", "Summary", "Started at", "Duration", "Status").WithWriter(rb)
	openEntries := db.FindEntries(func(te *storage.TimeEntry) bool { return true })
	for _, entry := range openEntries {
		hostName := entry.GetIssue().GetProject().GetHost().GetName()
		projectAddress := entry.GetIssue().GetProject().GetAddress()
		issueID := entry.GetIssue().GetID()
		entryDate := entry.Date

		tbl.AddRow(hostName,
			projectAddress,
			issueID,
			entry.Summary,
			entryDate.Format("2006-01-02 15:04:05"),
			time.Duration.Round(time.Since(entryDate), time.Second),
			isOpenBoolToString(entry.IsOpen()))
	}
	tbl.Print()
	result := rb.String()
	return result
}

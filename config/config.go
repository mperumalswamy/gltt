// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package config

import (
	"os"
	"path/filepath"
	"sync"
)

var lock = &sync.Mutex{}

type config struct {
	*WorkDir
	*Parameters
	Version        string
	CommitID       string
	ClosestVersion string
	ChangeLog      string
	License        string
}

func newConfig() *config {
	return &config{
		WorkDir:        NewWorkDir(),
		Parameters:     NewParameters(),
		Version:        "",
		CommitID:       "",
		ClosestVersion: "",
		ChangeLog:      "",
		License:        "",
	}
}

var pConfig *config

func Config() *config {
	if pConfig == nil {
		lock.Lock()
		defer lock.Unlock()
		if pConfig == nil {
			pConfig = newConfig()
		}
	}

	return pConfig
}

func (c *config) IsValid() bool {
	return c.Token != ""
}

func (c *config) DbFilename() string {
	homeDir, _ := os.UserHomeDir()
	return filepath.Join(homeDir, ".gltt", "db.json")
}

// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package config

type Parameters struct {
	Token   string
	Summary string
}

func NewParameters() *Parameters {
	return &Parameters{
		Token:   "",
		Summary: "",
	}
}

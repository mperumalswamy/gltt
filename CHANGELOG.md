# gltt (GitLab Time Tracker)

## [Unreleased]

### Add
- Add configuration for devcontainers
- Add Copyright-notice to files

### Fix
- Fix SonarLint-issues
- Fix template for changelog-generation
- Fix typo in error-message regarding missing config-file
- Fix panic during push caused by non-existent issue


## v0.1.0-nelli - 2023-02-28
### Add
- Add cmd "push" to write closed time-entries to GitLab
- Add cmd "changelog" to view gltt's change-log
- Add cmd "version" to report version-number and commit-id
- Add automatic injection of version-number and commit-id into application
- Add Makefile to improve automation and documentation
- Add cmd "status" to get a list of currently open time-entries
- Add cmd "report" for showing all closed time-entries at the current issue
- Add automatic creation of ~/.gltt if it doesn't exist
- Add basic functionality for local time-tracking
- Add basic config-handling using viper
- Add retrieval of host, project-address and issue-number from working-dir

### Change
- Change name from "gltt-go" to "gltt

### Fix
- Fix bug in storage.TimeEntry.IsOpen()

### Refactor
- Refactor modules storage and config


[Unreleased]: https://gitlab.com/dse82/gltt/compare/v0.1.0-nelli...main

VERSION_TAG := $(shell git tag --points-at HEAD | grep "^v[[:digit:]]\+\.[[:digit:]].[[:digit:]]\+\-[[:alpha:]]\+" | head -n 1)
COMMIT_ID := $(shell git rev-parse --short HEAD)
CLOSEST_VERSION := $(shell git describe --tags --match "v[0-9].[0-9].[0-9]-*" --abbrev=0 HEAD)
TARGETOS ?= $(GOOS)
TARGETARCH ?= $(GOARCH)
build:
	go generate .
	GOOS=$(TARGETOS) GOARCH=$(TARGETARCH) go build -ldflags '-w -X main.VERSION=$(VERSION_TAG) -X main.CLOSEST_VERSION=$(CLOSEST_VERSION) -X main.COMMIT_ID=$(COMMIT_ID)' .
run:
	go generate .
	go run -ldflags '-X main.VERSION=$(VERSION_TAG) -X main.CLOSEST_VERSION=$(CLOSEST_VERSION) -X main.COMMIT_ID=$(COMMIT_ID)' .

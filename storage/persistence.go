// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package storage

import (
	"encoding/json"
	"errors"
	"os"
)

var ErrSerializingDb = errors.New("unable to serialize db")
var ErrDeserializingDb = errors.New("unable to deserialize db")
var ErrWritingDb = errors.New("unable to write db to file")
var ErrReadDb = errors.New("unable to read db from file")

func (db *DB) Save(filename string) error {
	bytes, err := json.MarshalIndent(db, "", "  ")
	if err != nil {
		return errors.Join(ErrSerializingDb, err)
	}
	configDirPath := os.ExpandEnv("$HOME/.gltt")
	err = os.MkdirAll(configDirPath, os.ModePerm)
	if err != nil {
		return errors.Join(ErrWritingDb, err)
	}
	err = os.WriteFile(filename, bytes, 0644)
	if err != nil {
		return errors.Join(ErrWritingDb, err)
	}
	return nil
}

func (db *DB) Load(filename string) (*DB, error) {
	bytes, err := os.ReadFile(filename)
	if err != nil {
		return nil, errors.Join(ErrReadDb, err)
	}
	err = json.Unmarshal([]byte(bytes), db)
	if err != nil {
		return nil, errors.Join(ErrDeserializingDb, err)
	}
	return db, nil
}

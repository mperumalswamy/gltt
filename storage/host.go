// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package storage

import (
	"encoding/json"
	"errors"
)

var ErrProjectAlreadyExists = errors.New("project already exists at host")
var ErrProjectDoesNotExist = errors.New("project does not exist at host")

type Host struct {
	name     string
	projects map[string]*Project
	db       *DB
}

func NewHost(name string) *Host {
	return &Host{
		projects: make(map[string]*Project),
		name:     name,
	}
}

func (h *Host) UnmarshalJSON(b []byte) error {
	var tmp struct {
		Projects map[string]*Project
	}
	err := json.Unmarshal(b, &tmp)
	if err != nil {
		return err
	}
	h.projects = tmp.Projects
	for address, p := range h.projects {
		p.host = h
		p.address = address
	}
	return nil
}

func (h *Host) MarshalJSON() ([]byte, error) {
	type tmp struct {
		Projects map[string]*Project
	}

	return json.Marshal(tmp{
		Projects: h.projects,
	})
}

func (h *Host) AddProject(p *Project) error {
	if h.projects == nil {
		h.projects = make(map[string]*Project)
	}

	if h.projects[p.address] != nil {
		return ErrProjectAlreadyExists
	}
	h.projects[p.address] = p
	return nil
}

func (h *Host) GetProject(address string) (*Project, error) {
	p := h.projects[address]
	if p == nil {
		return nil, ErrProjectDoesNotExist
	}
	return p, nil
}

func (h *Host) GetName() string {
	return h.name
}

func (h *Host) GetDB() *DB {
	return h.db
}

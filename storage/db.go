// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package storage

import (
	"encoding/json"
	"errors"
)

var ErrHostAlreadyExists = errors.New("host already exists in db")
var ErrHostDoesNotExist = errors.New("host does not exist in db")

type DB struct {
	hosts map[string]*Host
}

func NewDB() *DB {
	return &DB{
		hosts: make(map[string]*Host),
	}
}

func (db *DB) String() string {
	bytes, _ := json.MarshalIndent(db, "", "  ")
	return string(bytes)
}

func (db *DB) UnmarshalJSON(b []byte) error {
	var tmp struct {
		Hosts map[string]*Host
	}
	err := json.Unmarshal(b, &tmp)
	if err != nil {
		return err
	}
	db.hosts = tmp.Hosts
	for hn, h := range db.hosts {
		h.db = db
		h.name = hn
	}
	return nil
}

func (db *DB) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		Hosts map[string]*Host
	}{
		Hosts: db.hosts,
	})
}

func (db *DB) AddHost(h *Host) error {
	if db.hosts == nil {
		db.hosts = make(map[string]*Host)
	}
	if db.hosts[h.name] != nil {
		return ErrHostAlreadyExists
	}
	db.hosts[h.name] = h
	return nil
}

func (db *DB) GetHost(name string) (*Host, error) {
	h := db.hosts[name]
	if h == nil {
		return nil, ErrHostDoesNotExist
	}
	return h, nil
}

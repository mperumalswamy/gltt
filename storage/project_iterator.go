// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package storage

type ProjectIterator struct {
	addresses  []string
	addressIdx int
	projects   map[string]*Project
}

func (h *Host) NewIterator() *ProjectIterator {
	addresses := make([]string, 0, len(h.projects))
	for address := range h.projects {
		addresses = append(addresses, address)
	}

	iterator := &ProjectIterator{
		projects:   h.projects,
		addresses:  addresses,
		addressIdx: -1,
	}

	return iterator
}

func (pi *ProjectIterator) HasNext() bool {
	return (len(pi.addresses) > pi.addressIdx+1)
}

func (pi *ProjectIterator) Next() *Project {
	pi.addressIdx += 1
	return pi.projects[pi.addresses[pi.addressIdx]]
}

// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package storage

import (
	"time"

	"github.com/google/uuid"
)

type TimeEntry struct {
	Summary  string
	Date     time.Time
	Duration time.Duration
	issue    *Issue
	id       uuid.UUID
}

func NewTimeEntry(date time.Time, duration time.Duration, summary string) *TimeEntry {
	return &TimeEntry{
		Date:     date,
		Summary:  summary,
		Duration: duration,
		id:       uuid.New(),
	}
}

func (te *TimeEntry) IsOpen() bool {
	return te.Duration <= 0
}

func (te *TimeEntry) GetIssue() *Issue {
	return te.issue
}

func (te *TimeEntry) GetIdx() uuid.UUID {
	return te.id
}

// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package storage

type IssueIterator struct {
	ids    []int
	idIdx  int
	issues map[int]*Issue
}

func (p *Project) NewIterator() *IssueIterator {
	ids := make([]int, 0, len(p.issues))
	for id := range p.issues {
		ids = append(ids, id)
	}

	iterator := &IssueIterator{
		issues: p.issues,
		ids:    ids,
		idIdx:  -1,
	}

	return iterator
}

func (ii *IssueIterator) HasNext() bool {
	return (len(ii.ids) > ii.idIdx+1)
}

func (ii *IssueIterator) Next() *Issue {
	ii.idIdx += 1
	return ii.issues[ii.ids[ii.idIdx]]
}

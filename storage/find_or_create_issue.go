// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package storage

func (db *DB) FindOrCreateIssue(hostName string, projectAddress string, issueID int) *Issue {
	host := db.hosts[hostName]
	if host == nil {
		host = NewHost(hostName)
		_ = db.AddHost(host)
	}

	project := host.projects[projectAddress]
	if project == nil {
		project = NewProject(projectAddress)
		_ = host.AddProject(project)
	}

	issue := project.issues[issueID]
	if issue == nil {
		issue = NewIssue()
		project.issues[issueID] = issue
	}

	return issue
}

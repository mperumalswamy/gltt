// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package storage

type HostIterator struct {
	names   []string
	nameIdx int
	hosts   map[string]*Host
}

func (db *DB) NewIterator() *HostIterator {
	names := make([]string, 0, len(db.hosts))
	for name := range db.hosts {
		names = append(names, name)
	}

	iterator := &HostIterator{
		hosts:   db.hosts,
		names:   names,
		nameIdx: -1,
	}

	return iterator
}

func (hi *HostIterator) HasNext() bool {
	return (len(hi.names) > hi.nameIdx+1)
}

func (hi *HostIterator) Next() *Host {
	hi.nameIdx += 1
	return hi.hosts[hi.names[hi.nameIdx]]
}

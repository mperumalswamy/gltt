// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package storage

import "github.com/google/uuid"

type TimeEntryIterator struct {
	ids         []uuid.UUID
	idIdx       int
	timeEntries map[uuid.UUID]*TimeEntry
}

func (i *Issue) NewIterator() *TimeEntryIterator {
	ids := make([]uuid.UUID, 0, len(i.entries))
	for id := range i.entries {
		ids = append(ids, id)
	}

	iterator := &TimeEntryIterator{
		timeEntries: i.entries,
		ids:         ids,
		idIdx:       -1,
	}

	return iterator
}

func (tei *TimeEntryIterator) HasNext() bool {
	return (len(tei.ids) > tei.idIdx+1)
}

func (tei *TimeEntryIterator) Next() *TimeEntry {
	tei.idIdx += 1
	return tei.timeEntries[tei.ids[tei.idIdx]]
}

func (tei *TimeEntryIterator) GetIdx() uuid.UUID {
	return tei.ids[tei.idIdx]
}

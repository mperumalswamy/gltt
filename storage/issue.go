// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package storage

import (
	"encoding/json"
	"errors"
	"strings"
	"time"

	"github.com/google/uuid"
)

var ErrNoRunningTimeEntry = errors.New("found no open time-entry to stop")
var ErrTimeEntryAlreadyRunning = errors.New("issue already has an open time-entry")
var ErrNoSummary = errors.New("cannot stop time-entry without summary")

type Issue struct {
	entries map[uuid.UUID]*TimeEntry
	project *Project
	id      int
}

func NewIssue() *Issue {
	return &Issue{
		entries: make(map[uuid.UUID]*TimeEntry),
	}
}

func (i *Issue) AddTimeEntry(timeEntry *TimeEntry) (*TimeEntry, error) {
	if FindFirst[TimeEntry](i.NewIterator(), func(entry *TimeEntry) bool { return (entry.Duration == 0) }) != nil {
		return nil, ErrTimeEntryAlreadyRunning
	}

	i.entries[timeEntry.id] = timeEntry
	return timeEntry, nil
}

func (i *Issue) FindOpenTimeEntry() *TimeEntry {
	openTimeEntry := FindFirst[TimeEntry](i.NewIterator(), func(entry *TimeEntry) bool { return (entry.Duration == 0) })
	return openTimeEntry
}

func (i *Issue) StartNewTimeEntry(summary string) (*TimeEntry, error) {
	timeEntry, err := i.AddTimeEntry(NewTimeEntry(time.Now(), time.Duration(0), summary))

	if err != nil {
		return nil, err
	}

	return timeEntry, nil
}

func (i *Issue) StopRunningTimeEntry(summary string) (*TimeEntry, error) {
	timeEntry := i.FindOpenTimeEntry()
	if timeEntry == nil {
		return nil, ErrNoRunningTimeEntry
	}

	trimmedSummary := strings.TrimSpace(summary)

	if len(trimmedSummary) == 0 {
		if len(strings.TrimSpace(timeEntry.Summary)) == 0 {
			return nil, ErrNoSummary
		}
	} else {
		timeEntry.Summary = trimmedSummary
	}

	timeEntry.Duration = time.Since(timeEntry.Date)
	return timeEntry, nil
}

func (i *Issue) MarshalJSON() ([]byte, error) {
	type tmp struct {
		Entries map[uuid.UUID]*TimeEntry
	}

	return json.Marshal(tmp{
		Entries: i.entries,
	})
}

func (i *Issue) UnmarshalJSON(b []byte) error {
	var tmp struct {
		Entries map[uuid.UUID]*TimeEntry
	}
	err := json.Unmarshal(b, &tmp)
	if err != nil {
		return err
	}
	i.entries = tmp.Entries
	for k, e := range i.entries {
		e.issue = i
		e.id = k
	}
	return nil
}

func (i *Issue) GetID() int {
	return i.id
}

func (i *Issue) GetProject() *Project {
	return i.project
}

func (i *Issue) DeleteEntry(id uuid.UUID) {
	delete(i.entries, id)
}

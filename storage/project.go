// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package storage

import (
	"encoding/json"
	"errors"
)

var ErrIssueAlreadyExists = errors.New("issue already exists at project")
var ErrIssueDoesNotExist = errors.New("issue does not exist at project")

type Project struct {
	issues  map[int]*Issue
	host    *Host
	address string
}

func NewProject(address string) *Project {
	return &Project{
		issues:  make(map[int]*Issue),
		address: address,
	}
}

func (p *Project) UnmarshalJSON(b []byte) error {
	var tmp struct {
		Issues map[int]*Issue
	}
	err := json.Unmarshal(b, &tmp)
	if err != nil {
		return err
	}
	p.issues = tmp.Issues
	for id, i := range p.issues {
		i.project = p
		i.id = id
	}
	return nil
}

func (p *Project) MarshalJSON() ([]byte, error) {
	type tmp struct {
		Issues map[int]*Issue
	}

	return json.Marshal(tmp{
		Issues: p.issues,
	})
}

func (p *Project) AddIssue(i *Issue) error {
	if p.issues == nil {
		p.issues = make(map[int]*Issue)
	}
	if p.issues[i.id] != nil {
		return ErrIssueAlreadyExists
	}
	p.issues[i.id] = i
	return nil
}

func (p *Project) GetIssue(id int) (*Issue, error) {
	i := p.issues[id]
	if i == nil {
		return nil, ErrIssueDoesNotExist
	}
	return i, nil
}

func (p *Project) GetAddress() string {
	return p.address
}

func (p *Project) GetHost() *Host {
	return p.host
}

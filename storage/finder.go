// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package storage

type Iterator[V any /*, W any*/] interface {
	HasNext() bool
	Next() *V
	//GetIdx() W
}

/*
	func FindFirst[V any, W any](iterator Iterator[V, W], matcher func(V) bool) *W {
		for iterator.HasNext() {
			if matcher(iterator.Next()) {
				result := iterator.GetIdx()
				return &result
			}
		}
		return nil
	}
*/
func FindFirst[V any](iterator Iterator[V], matcher func(*V) bool) *V {
	for iterator.HasNext() {
		thisItem := iterator.Next()
		if matcher(thisItem) {
			return thisItem
		}
	}
	return nil
}

func FindAll[V any](iterator Iterator[V], matcher func(*V) bool) []*V {
	result := make([]*V, 0)
	for iterator.HasNext() {
		thisItem := iterator.Next()
		if matcher(thisItem) {
			result = append(result, thisItem)
		}
	}
	return result
}

func (db *DB) GetAllEntries() []*TimeEntry {
	result := make([]*TimeEntry, 0)
	hi := db.NewIterator()
	for hi.HasNext() {
		pi := hi.Next().NewIterator()
		for pi.HasNext() {
			ii := pi.Next().NewIterator()
			for ii.HasNext() {
				for tei := ii.Next().NewIterator(); tei.HasNext(); {
					result = append(result, tei.Next())
				}
			}
		}
	}
	return result
}

func (db *DB) FindEntries(matcher func(*TimeEntry) bool) []*TimeEntry {
	result := make([]*TimeEntry, 0)
	hi := db.NewIterator()
	for hi.HasNext() {
		pi := hi.Next().NewIterator()
		for pi.HasNext() {
			ii := pi.Next().NewIterator()
			for ii.HasNext() {
				for tei := ii.Next().NewIterator(); tei.HasNext(); {
					te := tei.Next()
					if matcher(te) {
						result = append(result, te)
					}
				}
			}
		}
	}
	return result
}

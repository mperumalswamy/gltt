// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package cmd

import (
	config "dse/gltt/config"
	"fmt"

	"github.com/spf13/cobra"
)

// Provides a cobra.Command that outputs the version of this software.
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print out the version of this software",
	Long:  `Prints out the version of this software.`,
	Run: func(cmd *cobra.Command, args []string) {
		myVer := config.Config().Version
		if len(myVer) > 0 {
			fmt.Println("Version " + config.Config().Version + " (" + config.Config().CommitID + ")")
		} else {
			fmt.Println("Basierend auf Version " + config.Config().ClosestVersion + " (" + config.Config().CommitID + ")")
		}
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}

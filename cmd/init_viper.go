// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package cmd

import (
	"os"

	"github.com/spf13/viper"
)

// Initializes the passed in token with the value from the config-file or the
// env-var GLTT_TOKEN if the token is empty. The token is *not* empty (and
// therefore not set by this function) if a token was passed in via the
// command-line option --token.
func InitViper(token *string) {
	if *token != "" {
		return
	}

	// Viper: Load configuration from file
	viper.SetConfigName("config")
	configPath := "$HOME/.gltt"
	_ = os.Mkdir(configPath, os.ModePerm)
	viper.AddConfigPath(configPath)
	err := viper.ReadInConfig()
	if err != nil {
		Log().Println("Error occurred during reading of token from configuration: " + err.Error())
	}

	// Viper: Make token settable from command-line
	viper.SetEnvPrefix("GLTT")
	_ = viper.BindEnv("TOKEN")

	*token = viper.GetString("token")
}

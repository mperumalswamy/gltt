// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package cmd

import (
	config "dse/gltt/config"
	"fmt"

	"github.com/spf13/cobra"
)

// changelogCmd is a cobra.Command that outputs the changelog of the installed
// version of this software.
var changelogCmd = &cobra.Command{
	Use:   "changelog",
	Short: "Show changelog of the installed version of this software",
	Long: `Outputs the changelog of the gltt-version that is being executed. The changelog is output in markdown-format,
so the usage of a markdown-pager like e.g. glow (https://github.com/charmbracelet/glow) is recommended.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(config.Config().ChangeLog)
	},
}

func init() {
	rootCmd.AddCommand(changelogCmd)
}

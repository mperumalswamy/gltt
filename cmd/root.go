// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package cmd

import (
	config "dse/gltt/config"
	"os"

	"github.com/spf13/cobra"
)

// Provides the root cobra.Command, that is the entry point for the command line
// if no other command is given. rootCmd also provides the persistent flags,
// that can be used by all subcommands.
var rootCmd = &cobra.Command{
	Use:   "gltt-go",
	Short: "A brief description of your application",
	Long: `gltt is a command line tool to track the time you spend on GitLab-Issues and
allows you to sync them to Tyme as well as back to GitLab.

This program comes with ABSOLUTELY NO WARRANTY.
It is free software, and you are welcome to redistribute it.
To read the terms of license please use the command 'license'`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().StringVarP(&config.Config().Token, "token", "t", "", "Token for GitLab-authentication")
	rootCmd.PersistentFlags().StringVarP(&config.Config().Host, "host", "u", "", "Host of the GitLab-instance")
	rootCmd.PersistentFlags().IntVarP(&config.Config().IssueID, "issue", "i", -1, "Id of the issue you are working on")
	rootCmd.PersistentFlags().StringVarP(&config.Config().ProjectAddress, "project", "p", "", "Address of the "+
		"project you are working on.\nEither the GitLab-internal project-ID or in the form of namespace/project-name.")
}

// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package cmd

import (
	"dse/gltt/api"
	config "dse/gltt/config"
	"dse/gltt/gl"
	"dse/gltt/report"
	"fmt"

	"github.com/spf13/cobra"
)

// Provides a cobra.Command that outputs a report of the time entries that are
// currently stored for the issue in GitLab.
var reportCmd = &cobra.Command{
	Use:   "report",
	Short: "Prints a report of the time entries for the current issue that are stored in GitLab",
	Long: `Retrieves the GitLab time-entries of the issue that is referenced by
the currently checked out branch and prints them out.`,
	Run: func(cmd *cobra.Command, args []string) {
		InitViper(&config.Config().Token)
		err := RetrieveGitConfigFromWorkDir(config.Config().WorkDir)
		if err != nil {
			Log().Fatalln(err.Error())
		}
		r := api.MakeRequests(config.Config().Host, config.Config().Token, Log())
		issue, err := gl.LoadIssueTimeEntries(r, config.Config().ProjectAddress, config.Config().IssueID)
		if err != nil {
			Log().Fatal(err.Error())
		}
		fmt.Println(report.AllEntriesAtIssue(*issue))
	},
}

func init() {
	rootCmd.AddCommand(reportCmd)
}

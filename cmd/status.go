// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package cmd

import (
	config "dse/gltt/config"
	"dse/gltt/report"
	"dse/gltt/storage"
	"fmt"
	"time"

	"github.com/spf13/cobra"
)

// Provides a cobra.Command that outputs the status of the time-tracking at the
// issue referenced by the currently checked-out git-branch.
var statusCmd = &cobra.Command{
	Use:   "status",
	Short: "Prints the local time-tracking status of the current issue",
	Long: `Prints an overview of the local time-tracking entries for the issue referenced
by the currently checked-out git-branch, including the currently running
time-entry.`,
	Run: func(cmd *cobra.Command, args []string) {
		err := RetrieveGitConfigFromWorkDir(config.Config().WorkDir)
		if err != nil {
			Log().Fatalln(err.Error())
		}

		db := checkFunctionCall(config.Config().DbFilename(), storage.NewDB().Load)
		entries := report.AllOpenEntries(db)
		fmt.Println(entries)

		host := checkFunctionCall(config.Config().Host, db.GetHost)
		project := checkFunctionCall(config.Config().ProjectAddress, host.GetProject)
		issue := checkFunctionCall(config.Config().IssueID, project.GetIssue)
		var openEntry *storage.TimeEntry = nil
		if issue != nil {
			openEntry = issue.FindOpenTimeEntry()
		}
		if openEntry != nil {
			fmt.Printf("You have an open time-entry at the issue you are working on here, running since %v (%v)\n",
				openEntry.Date.Format("2006-01-02 15:04:05"), time.Duration.Round(time.Since(openEntry.Date), time.Second))
		} else {
			fmt.Println("You have no open time-entry at this issue.")
		}
	},
}

func init() {
	rootCmd.AddCommand(statusCmd)
}

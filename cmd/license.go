// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package cmd

import (
	config "dse/gltt/config"
	"fmt"

	"github.com/spf13/cobra"
)

// Provides a cobra.Command that outputs the license of this software.
var licenseCmd = &cobra.Command{
	Use:   "license",
	Short: "Show the license of this software",
	Long:  "Shows the license of this software that governs the use and guarantees of this software",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(config.Config().License)
	},
}

func init() {
	rootCmd.AddCommand(licenseCmd)
}

// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package cmd

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"golang.org/x/term"
)

var (
	// configureCmd is a cobra.Command that starts an interactive process
	// guiding the user through the creation of the configuration-file.
	configureCmd = &cobra.Command{
		Use:   "configure",
		Short: "Start interactive creation of the configuration-file",
		Long:  `Starts an interactive process guiding you through the creation of the configuration-file.`,
		Run: func(cmd *cobra.Command, args []string) {

			// TODO Refactor this, so the different blocks are extracted in
			// functions and the amount of error-handling code is reduced.

			// Check if config-file already exists and exit if so.
			configFilePath := os.ExpandEnv("$HOME/.gltt/config.json")
			if _, err := os.Stat(configFilePath); err == nil {
				Log().Fatalln("Configuration file already exists at", configFilePath)
			} else if !os.IsNotExist(err) {
				Log().Fatalln("Failed to check if the configuration file exists:", err)
			}

			// Ask for permission to generate the configuration file and exit if
			// not granted.
			fmt.Println("The configuration will be stored in the file ~/.gltt/config.json (this file doesn't exist yet).")
			fmt.Println("Is it okay to create this file? (y/n)")
			var permissionToCreateConfigFile string
			_, err := fmt.Scanln(&permissionToCreateConfigFile)
			if err != nil {
				Log().Fatalln("Failed to read the answer:", err)
			} else if (permissionToCreateConfigFile != "y") && (permissionToCreateConfigFile != "Y") {
				Log().Fatalln("Aborting.")
			}

			// Ask for the GitLab-token and exit if not provided.
			var token string

			fmt.Println("Please create an Access Token with the 'api' scope for the GitLab instance you want to interact with and enter it here: ")
			oldState, err := term.MakeRaw(int(os.Stdin.Fd()))
			if err != nil {
				Log().Fatalln("Failed to set terminal to raw mode:", err)
				os.Exit(1)
			}

			tokenBytes, err := term.ReadPassword(int(os.Stdin.Fd()))
			term.Restore(int(os.Stdin.Fd()), oldState)
			if err != nil {
				Log().Fatalln("Failed to read the token:", err)
			}
			token = string(tokenBytes)

			// Create the ~/.gltt directory if it doesn't exist.
			fmt.Println("creating the configuration directory at ~/.gltt…")
			configDirPath := os.ExpandEnv("$HOME/.gltt")
			err = os.MkdirAll(configDirPath, os.ModePerm)
			if err != nil {
				Log().Fatalln("Failed to create the configuration directory:", err)
			}

			// Create the configuration file with token as the only value.
			fmt.Println("Creating the configuration file at ~/.gltt/config.json…")
			configFile, err := os.Create(configFilePath)
			if err != nil {
				Log().Fatalln("Failed to create the configuration file:", err)
			}
			defer configFile.Close()

			configData := map[string]string{
				"token": token,
			}

			encoder := json.NewEncoder(configFile)
			err = encoder.Encode(configData)
			if err != nil {
				Log().Fatalln("Failed to write the token to the configuration file:", err)
			}

			fmt.Println("Configuration file successfully created at", configFilePath+".")
		},
	}
)

func init() {
	rootCmd.AddCommand(configureCmd)
}

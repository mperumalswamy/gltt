// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package cmd

import (
	config "dse/gltt/config"
	"dse/gltt/storage"

	"github.com/spf13/cobra"
)

// Provides a cobra.Command that starts a new time-entry. The command also
// supports the command-line flag --summary/-s to provide a summary for the
// time-entry.
var (
	startCmd = &cobra.Command{
		Use:   "start",
		Short: "Starts a timer for the current issue",
		Long:  `Creates a new TimeEntry for the currently checked out branch and starts it.`,
		Run: func(cmd *cobra.Command, args []string) {
			err := RetrieveGitConfigFromWorkDir(config.Config().WorkDir)
			if err != nil {
				Log().Fatalln(err.Error())
			}

			db, err := storage.NewDB().Load(config.Config().DbFilename())
			if err != nil {
				Log().Printf("%v – creating new database", err.Error())
				db = storage.NewDB()
			}
			issue := db.FindOrCreateIssue(config.Config().Host, config.Config().ProjectAddress, config.Config().IssueID)
			timeEntry := checkFunctionCall(config.Config().Summary, issue.StartNewTimeEntry)
			Log().Printf("Started new time-entry at %v/%v/%v at %v", config.Config().Host,
				config.Config().ProjectAddress, config.Config().IssueID, timeEntry.Date)
			checkProcedureCall(config.Config().DbFilename(), db.Save)
		},
	}
)

func init() {
	rootCmd.AddCommand(startCmd)
	startCmd.Flags().StringVarP(&config.Config().Summary, "summary", "s", "", "Message summarizing the time-entry.")
}

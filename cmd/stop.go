// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package cmd

import (
	config "dse/gltt/config"
	"dse/gltt/storage"

	"github.com/spf13/cobra"
)

// Provides a cobra.Command that stops any running time-entry of the current
// issue. Running Time-entries for other issues are not affected.
var (
	stopCmd = &cobra.Command{
		Use:   "stop",
		Short: "Stops the running time-entry of the current issue",
		Long: `Stops any running time-entry of the issue that is referenced by 
the currently checked out branch. Time-entries of other issues are not affected. `,
		Run: func(cmd *cobra.Command, args []string) {
			err := RetrieveGitConfigFromWorkDir(config.Config().WorkDir)
			if err != nil {
				Log().Fatalln(err.Error())
			}

			db := checkFunctionCall(config.Config().DbFilename(), storage.NewDB().Load)
			issue := db.FindOrCreateIssue(config.Config().Host, config.Config().ProjectAddress, config.Config().IssueID)
			timeEntry := checkFunctionCall(config.Config().Summary, issue.StopRunningTimeEntry)
			if err != nil {
				Log().Fatalln(err.Error())
			} else {
				Log().Printf("Stopped time-entry at %v/%v/%v at %v", config.Config().Host,
					config.Config().ProjectAddress, config.Config().IssueID, timeEntry.Date.Add(timeEntry.Duration))
				checkProcedureCall(config.Config().DbFilename(), db.Save)
			}
		},
	}
)

func init() {
	rootCmd.AddCommand(stopCmd)
	stopCmd.Flags().StringVarP(&config.Config().Summary, "summary", "s", "", "Message summarizing the time-entry.")
}

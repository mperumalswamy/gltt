// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package cmd

import (
	"dse/gltt/api"
	"time"

	config "dse/gltt/config"
	"dse/gltt/storage"

	"github.com/spf13/cobra"
)

const SCOPE_ISSUE = "issue"
const SCOPE_PROJECT = "project"
const SCOPE_HOST = "host"
const SCOPE_DB = "db"

// Provides a cobra.Command that pushes stopped logged time-entries to GitLab.
var (
	/*scope       string validScopes = []string{SCOPE_ISSUE, SCOPE_PROJECT,
	SCOPE_HOST, SCOPE_DB}*/
	pushCmd = &cobra.Command{
		Use:   "push",
		Short: "Pushes the stopped logged time-entries to GitLab",
		Long: `Filters all stopped time-entries from the database and pushes
them to their respective GitLab-issues.

At a later stage scoped-pushing is planned, so that only entries belonging to
a certain issue, project, host or database are pushed.`,
		Run: func(cmd *cobra.Command, args []string) {
			InitViper(&config.Config().Token)
			checkProcedureCall(config.Config().WorkDir, RetrieveGitConfigFromWorkDir)

			/*scope := strings.ToLower(strings.TrimSpace(scope)) if scope == ""
			{ scope = SCOPE_ISSUE
			}
			if slices.Index(validScopes, scope) == -1 { Log().Fatalf("Scope %v
			    unknown – Please use one of %v", scope, validScopes)
			}*/

			db := checkFunctionCall(config.Config().DbFilename(), storage.NewDB().Load)
			host := checkFunctionCall(config.Config().Host, db.GetHost)
			project := checkFunctionCall(config.Config().ProjectAddress, host.GetProject)
			issue := checkFunctionCall(config.Config().IssueID, project.GetIssue)
			requests := api.MakeRequests(config.Config().Host, config.Config().Token, Log())
			issuableID, err := requests.FetchIssuableID(config.Config().ProjectAddress, config.Config().IssueID)
			if err != nil {
				Log().Fatalln(err.Error())
			}

			closedEntries := storage.FindAll[storage.TimeEntry](issue.NewIterator(), func(te *storage.TimeEntry) bool {
				return !te.IsOpen()
			})

			sClosedEntries := len(closedEntries)

			if sClosedEntries == 0 {
				Log().Fatalln("No stopped entries to push.")
			}

			for idxClosedEntry, closedEntry := range closedEntries {
				Log().Printf("(%v/%v) Pushing closed entry to GitLab: %v: %v (%v)", idxClosedEntry+1, sClosedEntries,
					closedEntry.Date, closedEntry.Duration.Round(time.Second), closedEntry.Summary)
				err := requests.CreateTimeLogEntry(issuableID, closedEntry.Date, closedEntry.Duration, closedEntry.Summary)
				if err != nil {
					Log().Fatalln(err.Error())
				} else {
					issue.DeleteEntry(closedEntry.GetIdx())
					checkProcedureCall(config.Config().DbFilename(), db.Save)
				}
			}
		},
	}
)

func init() {
	rootCmd.AddCommand(pushCmd)
	//pushCmd.Flags().StringVarP(&scope, "scope", "s", "", "Scope for pushing")
}

// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package cmd

import (
	"dse/gltt/config"
	"dse/gltt/gt"
	"errors"

	"github.com/go-git/go-git/v5"
)

var ErrUnableToOpenGitRepo = errors.New("unable to open git-repository")
var ErrUnableToExtractHostName = errors.New("unable to extract host-name from working-dir, " +
	"please pass via the flag --host")
var ErrUnableToExtractProjectAddress = errors.New("unable to extract project-address from working-dir, " +
	"please pass via flag --project")
var ErrUnableToExtractIssueId = errors.New("unable to extract issue-Id from working-dir, please pass via flag --issue")
var ErrUnableToExtractOriginURL = errors.New("unable to extract origin url from working-dir, please pass --host, " +
	"--project and --isue")

// Retrieves the host-name, the project-address and the issue-id from the
// git-configuration of the current working-dir and stores them in the passed in
// workDir-configuration. If the host-name, the project-address or the issue-id
// have already been set by the user via the command-line, they are not overwritten.
func RetrieveGitConfigFromWorkDir(workDir *config.WorkDir) error {
	repo, err := git.PlainOpen(".")
	if err != nil {
		return errors.Join(ErrUnableToOpenGitRepo, err)
	}

	originURL, err := gt.FetchOriginURL(repo)
	if err != nil {
		return errors.Join(ErrUnableToExtractOriginURL, err)
	}

	if workDir.Host == "" {
		workDir.Host, err = gt.ExtractHostName(*originURL)
		if err != nil {
			return errors.Join(ErrUnableToExtractHostName, err)
		}
	}

	if workDir.ProjectAddress == "" {
		workDir.ProjectAddress, err = gt.ExtractProjectAddress(*originURL)
		if err != nil {
			return errors.Join(ErrUnableToExtractProjectAddress, err)
		}
	}

	if workDir.IssueID == -1 {
		workDir.IssueID, err = gt.ExtractIssueID(repo)
		if err != nil {
			return errors.Join(ErrUnableToExtractIssueId, err)
		}
	}
	return nil
}

// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package cmd

// checkFunctionCall calls a function with a value and checks if the function
// returned an error. If so, the error is logged and the program is terminated.
// Otherwise the result of the function is returned.
func checkFunctionCall[V any, W any](value V, f func(V) (W, error)) W {
	result, err := f(value)
	if err != nil {
		Log().Fatalln(err.Error())
	}
	return result
}

// checkProcedureCall calls a function with a value and checks if the function
// returned an error. If so, the error is logged and the program is terminated.
func checkProcedureCall[V any](value V, f func(V) error) {
	err := f(value)
	if err != nil {
		Log().Fatalln(err.Error())
	}
}

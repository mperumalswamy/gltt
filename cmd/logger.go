// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package cmd

import (
	"log"
	"sync"
)

var logger *log.Logger
var lock = &sync.Mutex{}

// Provides package-wide access to the logger.
func Log() *log.Logger {
	if logger == nil {
		lock.Lock()
		defer lock.Unlock()
		if logger == nil {
			logger = log.Default()
		}
	}

	return logger
}

// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package gt

import (
	"errors"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"github.com/go-git/go-git/v5"
)

var ErrUnableToRetrieveOriginUrl = errors.New("unable to retrieve URL of remote 'origin' from repo")
var ErrUnableToRetrieveHead = errors.New("unable to retrieve HEAD reference from repo")
var ErrHeadIsNoBranch = errors.New("checked out HEAD is not a branch")
var ErrUnableToExtractIssueId = errors.New("unable to extract issue-id from branch-name")
var ErrUnableToExtractProjectAddress = errors.New("unable to extract project-address from origin-URL")
var ErrUnableToExtractHostName = errors.New("unable to extract host-name from origin-URL")

func FetchOriginURL(repo *git.Repository) (*url.URL, error) {
	remote, err := repo.Remote("origin")
	if err != nil {
		return nil, errors.Join(ErrUnableToRetrieveOriginUrl, err)
	}
	return url.Parse(remote.Config().URLs[0])
}

func ExtractIssueID(repo *git.Repository) (int, error) {
	ref, err := repo.Head()
	if err != nil {
		return -1, errors.Join(ErrUnableToRetrieveHead, err)
	}
	if !ref.Name().IsBranch() {
		return -1, ErrHeadIsNoBranch
	}
	branchName := ref.Name().Short()
	re := regexp.MustCompile(`(\d+)-.*`)
	res := re.FindStringSubmatch(branchName)
	if res == nil {
		return -1, ErrUnableToExtractIssueId
	}
	issueId, _ := strconv.Atoi(res[1])
	return issueId, nil
}

func ExtractProjectAddress(originURL url.URL) (string, error) {
	projectAddress := strings.Trim(strings.TrimSuffix(originURL.Path, ".git"), "/")
	if len(projectAddress) == 0 {
		return "", ErrUnableToExtractProjectAddress
	}
	return projectAddress, nil
}

func ExtractHostName(originURL url.URL) (string, error) {
	host := originURL.Host
	if len(host) == 0 {
		return "", ErrUnableToExtractHostName
	}
	return host, nil
}

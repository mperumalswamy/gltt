// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package api

import (
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	_ "github.com/Khan/genqlient/generate"
	"github.com/Khan/genqlient/graphql"
	_ "github.com/vektah/gqlparser/v2/validator"
)

//go:generate apollo client:download-schema --endpoint=https://gitlab.com/api/graphql schema.graphql
//go:generate go run github.com/Khan/genqlient

var ErrNoIssuableFound = errors.New("unable to find issuable matching issue-ID")

type Requests struct {
	transport authedTransport
	url       string
}

type Logger interface {
	Println(v ...interface{})
}

func MakeRequests(hostName string, token string, logger Logger) Requests {
	if len(token) == 0 && logger != nil {
		logger.Println(`No token provided. Please provide a token using the --token flag or through the configuration. With no token provided, gltt will only be able to access public resources in GitLab.`)
	}
	return Requests{
		transport: authedTransport{
			wrapped: http.DefaultTransport,
			token:   token,
		},
		url: "https://" + hostName + "/api/graphql",
	}
}

type authedTransport struct {
	wrapped http.RoundTripper
	token   string
}

func (t *authedTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	req.Header.Set("Authorization", "bearer "+t.token)
	return t.wrapped.RoundTrip(req)
}

func (r *Requests) FetchIssueTimeLog(projectAddress string, issueID string) (*getIssueTimeLogResponse, error) {
	client := graphql.NewClient(r.url, &http.Client{Transport: &r.transport})

	return getIssueTimeLog(context.Background(), client, projectAddress, issueID)
}

func (r *Requests) CreateTimeLogEntry(issuableID string, date time.Time, duration time.Duration, summary string) error {
	client := graphql.NewClient(r.url, &http.Client{Transport: &r.transport})
	timeSpent := strconv.Itoa(int(duration.Seconds())) + "s"
	_, err := createTimeLogEntry(context.Background(), client, issuableID, date, timeSpent, summary)
	if err != nil {
		return err
	} else {
		return nil
	}
}

func (r *Requests) FetchIssuableID(projectAddress string, issueID int) (string, error) {
	client := graphql.NewClient(r.url, &http.Client{Transport: &r.transport})
	response, err := getIssuableId(context.Background(), client, projectAddress, strconv.Itoa(issueID))
	if err != nil {
		return "", err
	} else {
		if len(response.Project.Issues.Nodes) == 0 {
			return "", ErrNoIssuableFound
		} else {
			return response.Project.Issues.Nodes[0].Id, nil
		}
	}
}

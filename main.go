// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package main

import (
	"dse/gltt/cmd"
	. "dse/gltt/config"
	_ "embed"
	"fmt"
)

var COMMIT_ID = "unset"
var VERSION = "unset"
var CLOSEST_VERSION = "unset"

//go:generate go run github.com/git-chglog/git-chglog/cmd/git-chglog@latest --output CHANGELOG.md
//go:embed CHANGELOG.md
var CHANGELOG string

//go:embed LICENSE
var LICENSE string

func main() {
	fmt.Println("Copyright (C) 2024 the Dietmar Seth Esch. All rights reserved.")
	fmt.Println()
	Config().CommitID = COMMIT_ID
	Config().Version = VERSION
	Config().ClosestVersion = CLOSEST_VERSION
	Config().ChangeLog = CHANGELOG
	Config().License = LICENSE
	cmd.Execute()
}

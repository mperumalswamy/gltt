// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package gl

import (
	"dse/gltt/storage"
	"time"
)

type TimeEntry struct {
	storage.TimeEntry
	User string
}

func NewTimeEntry(date time.Time, duration time.Duration, user string, summary string) *TimeEntry {
	return &TimeEntry{
		TimeEntry: storage.TimeEntry{
			Date:     date,
			Summary:  summary,
			Duration: duration,
		},
		User: user,
	}
}

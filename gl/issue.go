// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package gl

import "time"

type Issue struct {
	Entries    []*TimeEntry
	Estimate   time.Duration
	TotalSpent time.Duration
}

func (i *Issue) AddTimeEntry(te *TimeEntry) {
	i.Entries = append(i.Entries, te)
}

func (i *Issue) RemoveTimeEntry(te *TimeEntry) {
	// Not implemented yet
}

func NewIssue() Issue {
	return Issue{
		Entries:    make([]*TimeEntry, 0),
		Estimate:   0,
		TotalSpent: 0,
	}
}

// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package gl

import (
	"dse/gltt/api"
	"errors"
	"strconv"
	"time"
)

var ErrUnableToRetrieveTimeLogs = errors.New("graphql-query failed")
var ErrNoIssueFound = errors.New("no matching issue found")

const NANOSECONDS_PER_SECOND = 1_000_000_000

func LoadIssueTimeEntries(r api.Requests, projectAddress string, issueID int) (*Issue, error) {
	result := NewIssue()

	response, err := r.FetchIssueTimeLog(projectAddress, strconv.Itoa(issueID))

	if err != nil {
		return nil, errors.Join(ErrUnableToRetrieveTimeLogs, err)
	}

	issues := response.Project.Issues.Nodes
	if len(issues) == 0 {
		return nil, ErrNoIssueFound
	}
	result.Estimate = time.Duration(issues[0].TimeEstimate * NANOSECONDS_PER_SECOND)

	timelogs := issues[0].Timelogs

	totalSpentTime, err := strconv.Atoi(timelogs.TotalSpentTime)
	if err != nil {
		return nil, err
	}

	result.TotalSpent = time.Duration(totalSpentTime * NANOSECONDS_PER_SECOND)

	for _, timelogEntry := range timelogs.Nodes {
		result.Entries = append(result.Entries,
			NewTimeEntry(timelogEntry.SpentAt, time.Duration(timelogEntry.TimeSpent*NANOSECONDS_PER_SECOND),
				timelogEntry.User.Name, timelogEntry.Summary))
	}

	return &result, nil
}

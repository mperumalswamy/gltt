// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package gl

import (
	"regexp"
	"strconv"
	"time"
)

const MINUTE_IN_NANOSECONDS = 60_000_000_000

var conversionTable = map[string]int{
	"m":       MINUTE_IN_NANOSECONDS,
	"minute":  MINUTE_IN_NANOSECONDS,
	"minutes": MINUTE_IN_NANOSECONDS,
	"h":       MINUTE_IN_NANOSECONDS * 60,
	"hour":    MINUTE_IN_NANOSECONDS * 60,
	"hours":   MINUTE_IN_NANOSECONDS * 60,
	"d":       MINUTE_IN_NANOSECONDS * 60 * 8,
	"day":     MINUTE_IN_NANOSECONDS * 60 * 8,
	"days":    MINUTE_IN_NANOSECONDS * 60 * 8,
	"w":       MINUTE_IN_NANOSECONDS * 60 * 8 * 5,
	"week":    MINUTE_IN_NANOSECONDS * 60 * 8 * 5,
	"weeks":   MINUTE_IN_NANOSECONDS * 60 * 8 * 5,
	"mo":      MINUTE_IN_NANOSECONDS * 60 * 8 * 5 * 4,
	"month":   MINUTE_IN_NANOSECONDS * 60 * 8 * 5 * 4,
	"months":  MINUTE_IN_NANOSECONDS * 60 * 8 * 5 * 4,
}

func ConvertGlToGoDuration(glTime string) (time.Duration, error) {
	durationRE := regexp.MustCompile(`(\d+) ?(mo|month|months|w|week|weeks|d|day|days|h|hour|hours|m|minute|minutes)`)
	matchedDurations := durationRE.FindAllStringSubmatch(glTime, -1)

	nanoSeconds := int(0)

	for _, matchedDuration := range matchedDurations {
		v := matchedDuration[1]
		unit := matchedDuration[2]
		vInt, _ := strconv.Atoi(v)
		nanoSeconds += vInt * conversionTable[unit]
	}

	return time.Duration(nanoSeconds), nil
}

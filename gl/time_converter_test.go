// Copyright (C) 2024 Dietmar Seth Esch (mail@dsesch.de)
//
// This program is free software under the terms of the GNU General Public
// License version 3.
//
// If you did not receive a copy of the GNU General Public License along with
// this program in a file named 'LICENSE' see <https://www.gnu.org/licenses>.

package gl

import (
	"testing"
	"time"
)

func TestConvertToGoTime(t *testing.T) {
	testCases := map[string]time.Duration{
		"asf fas 1h asasf 12":      time.Duration(60 * MINUTE_IN_NANOSECONDS),
		"asf fas 1 h asasf 12":     time.Duration(60 * MINUTE_IN_NANOSECONDS),
		"asf fas 2h asasf 12":      time.Duration(120 * MINUTE_IN_NANOSECONDS),
		"asf fas 1h asasf 12m":     time.Duration(72 * MINUTE_IN_NANOSECONDS),
		"asf fas 1d 1h asasf 08m":  time.Duration((9*60 + 8) * MINUTE_IN_NANOSECONDS),
		"asf fas 1w 5h":            time.Duration(45 * 60 * MINUTE_IN_NANOSECONDS),
		"asf fas 1mo 2weeks 5h 3m": time.Duration((245*60 + 3) * MINUTE_IN_NANOSECONDS),
	}
	for humanFormat, goFormat := range testCases {
		conversionResult, err := ConvertGlToGoDuration(humanFormat)
		if err != nil {
			t.Errorf("Failed to parse '%v'", humanFormat)
		}
		if conversionResult != goFormat {
			t.Errorf("Parsing-result of '%v' is wrong: conversion-result it %v but should be %v",
				humanFormat,
				conversionResult,
				goFormat)
		}
	}
}
